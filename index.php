<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Egyptian Magic</title>
    <meta name="description" content="Egyptian Magic">
    <meta name="keywords" content="Egyptian Magic">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" media="screen, projection" href="css/owl.theme.default.min.css" />
    <link rel="stylesheet" type="text/css" media="screen, projection" href="css/main.css" />
    <link rel="stylesheet" type="text/css" href="css/media.css" />

</head>

<body>

    <div class="loader-outer">
        <div class="loader"></div>
    </div>

    <div class="container-outer">
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-6">
                        <div class="logo"><a href="javascript:;"><img src="images/logo.png"></a></div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-6">
                        <div class="mobile-menu">
                            <a href="javascript:;"><img src="images/trigger.png" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!--end header-->

        <div class="mega_menu">
            <div class="close-menu"><a href="javascript:;"><i class="fa fa-times" aria-hidden="true"></i></a></div>
            <ul>
                <li><a href="javascript:;">ABOUT</a></li>
                <li><a href="javascript:;">CELEBRITY BUZZ</a></li>
                <li><a href="javascript:;">BEST USES</a></li>
                <li><a href="javascript:;">STORE LOCATOR</a></li>
                <li><a href="javascript:;">FAQ</a></li>
                <li><a href="javascript:;">BUY NOW</a></li>
                <li><a href="javascript:;">CONTACT US</a></li>
            </ul>
        </div>


        <section class="banner">
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <div class="banner-caption">
                        <div class="banner-product"><img src="images/product.png"></div>
                        <div class="banner-caption-inner">
                            <h4>THE ORIGINAL</h4>
                            <h2>SINCE 1991</h2>
                            <p>Multi-purpose skin cream with a magical touch
                                Celebrities’ best-kept beauty secret for a beautiful and healthy skin.100% natural
                                formula.</p>
                            <div class="buy-now"><a href="javascript:;">Buy Now</a></div>
                        </div>
                        <div class="clr"></div>
                    </div>
                </div>
                <div class="item">
                    <div class="banner-caption">
                        <div class="banner-product"><img src="images/product.png"></div>
                        <div class="banner-caption-inner">
                            <h4>THE ORIGINAL</h4>
                            <h2>SINCE 1991</h2>
                            <p>Multi-purpose skin cream with a magical touch
                                Celebrities’ best-kept beauty secret for a beautiful and healthy skin.100% natural
                                formula.</p>
                            <div class="buy-now"><a href="javascript:;">Buy Now</a></div>
                        </div>
                        <div class="clr"></div>
                    </div>
                </div>
            </div>
        </section>
        <!--end banner-->

        <section class="product">
            <div class="container-fluid">

                <div class="product-inner">

                    <div class="row no-gutters">

                        <div class="col-md-7 flex-md-last product-image ">
                            <img src="images/product1.jpg">
                        </div>
                        <div class="col-md-5 product-description">
                            <div class="description-inner">
                                <h2>MULTIPURPOSE <br />CREAM</h2>
                                <p>Hydrates and repairs skin from head to toe. Use as a moisturizer, holistic acne
                                    treatment, lip balm, eye cream, after-sun moisturizer or even hair mask. It also
                                    works
                                    wonders on eczema and skin irritations.</p>
                                <div class="exp-more"><a href="javascript:;">Explore more</a></div>
                            </div>
                        </div>
                    </div>
                    <!--end row-->
                </div>
                <!--end product-inner-->
                <div class="product-inner">
                    <div class="row no-gutters">

                        <div class="col-md-7 product-image">
                            <img src="images/product2.jpg">
                        </div>
                        <div class="col-md-5 product-description">
                            <div class="description-inner">
                                <h2>ALL-NATURAL <br />INGREDIENTS</h2>
                                <p>100% NATURAL FORMULA. HANDMADE.
                                    Olive oil, Honey, Royal jelly, Propolis, Beeswax, Bee pollen.
                                </p>
                                <div class="exp-more"><a href="javascript:;">Explore more</a></div>
                            </div>
                        </div>
                    </div>
                    <!--end row-->
                </div>
                <!--end product-inner-->
            </div>
        </section>


        <section class="celebrities">
            <div class="container-fluid">
                <div class="celebrities-inner">
                    <div class="section-heading">CELEBRITIES FAVORITE</div>
                    <div class="row no-gutters">
                    <div class="col-md-12">
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="celebrities-images"><img src="images/cl1.jpg"></div>
                            </div>
                            <div class="item">
                                <div class="celebrities-images"><img src="images/cl2.jpg"></div>
                            </div>
                            <div class="item">
                                <div class="celebrities-images"><img src="images/cl3.jpg"></div>
                            </div>
                            <div class="item">
                                <div class="celebrities-images"><img src="images/cl4.jpg"></div>
                            </div>
                        </div>
                        </div>

                    </div>
                    <div class="row no-gutters">
                        <div class="col-md-12">
                            <div class="video-section">
                                <iframe width="100%" height="500" src="https://www.youtube.com/embed/B4v1LPNFPTg"
                                    frameborder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



        <section class="pin">
            <div class="container">
                <div class="section-heading">Locate store near by you</div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="pin-form">
                            <form id="pincode-form">
                                <h4>Enter Pin code</h4>
                                <div class="pin-input">
                                    <input type="text" name="pin" />
                                </div>
                                <div class="pin-button">
                                    <button type="submit">Enter</button>
                                </div>
                                <div class="clr"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="map">
            <img src="images/map.jpg">
        </section>


        <section class="feature">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-md-4">

                        <div class="feature-heading">
                            Featured <br />
                            in
                        </div>


                    </div>
                    <div class="col-md-8">
                        <div class="feature-slider">
                            <div class="owl-carousel owl-theme">
                                <div class="item">
                                    <img src="images/feature1.jpg">
                                </div>
                                <div class="item">
                                    <img src="images/feature2.jpg">
                                </div>
                                <div class="item">
                                    <img src="images/feature3.jpg">
                                </div>
                                <div class="item">
                                    <img src="images/feature1.jpg">
                                </div>
                                <div class="item">
                                    <img src="images/feature2.jpg">
                                </div>
                                <div class="item">
                                    <img src="images/feature3.jpg">
                                </div>
                                <div class="item">
                                    <img src="images/feature1.jpg">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="contact-us">
            <div class="container">
                <div class="section-heading">you’ll be heared</div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="contact-form">
                            <form id="contact-form">
                                <div class="input-field">
                                    <input type="text" name="name" placeholder="Name">
                                </div>
                                <div class="input-field">
                                    <input type="email" name="email" placeholder="Email">
                                </div>
                                <div class="input-field">
                                    <input type="text" name="phone" placeholder="Phone">
                                </div>
                                <div class="input-field">
                                    <textarea rows="4" cols="50" class="message" Placeholder="Message"></textarea>
                                </div>
                                <div class="submit">
                                    <div class="width50">
                                        <div class="form-group iagree12">
                                            <div class="checkbox custom-select13">
                                                <label><input type="checkbox" name="terms"
                                                        class="cs_check"><span></span>Sign up for
                                                    updates
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="width50">
                                        <div class="form-group iagree12">
                                            <button type="submit">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>




        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="copyright">
                            <div class="footer-logo"><a href="javascript:;"><img src="images/footer-logo.png"></a></div>
                            <div class="copy-content desktop">© 2019 Egyptian Magic - All Rights Reserved</div>
                        </div>

                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="social">
                            <ul>
                                <li><a href="javascript:;"><i class="fa fa-heart" aria-hidden="true"></i></a></li>
                                <li><a href="javascript:;"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                                <li><a href="javascript:;"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></li>
                                <li><a href="javascript:;"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            </ul>
                            <ul>
                                <li><a href="javascript:;"><i class="fa fa-phone" aria-hidden="true"></i></a></li>
                                <li><a href="javascript:;"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                                <li><a href="javascript:;"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
                                <li><a href="javascript:;"><i class="fa fa-snapchat-ghost" aria-hidden="true"></i></a>
                                </li>
                            </ul>

                        </div>


                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="copy-content mobile">© 2019 Egyptian Magic - All Rights Reserved</div>
                    </div>
                </div>


            </div>
        </footer>








    </div>
    <!--end container-->








    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script src="js/owl.carousel.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/init.js"></script>
</body>

</html>