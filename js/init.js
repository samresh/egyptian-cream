$(document).ready(function() {
    $('.banner .owl-carousel').owlCarousel({
      loop: true,
      margin: 0,
      responsiveClass: true,
      responsive: {
        0: {
          items: 1,
          nav: true
        },
        600: {
          items: 1,
          nav: false
        },
        1000: {
          items: 1,
          nav: true,
          loop: false,
          margin: 0
        }
      }
    })
  })

  $(document).ready(function() {
    $('.feature-slider .owl-carousel').owlCarousel({
      loop: false,
      margin: 10,
      items: 4,
      responsiveClass: true,
      responsive: {
        0: {
          items: 1,
          nav: true
        },
        600: {
          items: 2,
          nav: true
        },
        1000: {
          items: 3,
          nav: true,
          loop: false,
          margin: 20
        }
      }
    })
  })

  $(document).ready(function() {
    $('.celebrities .owl-carousel').owlCarousel({
      loop: false,
      nav: false,
      margin: 0,
      items: 4,
      responsiveClass: true,
      responsive: {
        0: {
          items: 1,
          nav: true
        },
        600: {
          items: 3,
          nav: true
        },
        1000: {
          items: 4,
          nav: false,
          loop: false,
          margin: 0
        }
      }
    })
  })


  $(window).load(function(){
    $('.loader-outer').fadeOut();
 });
 $(function(){
  $('.mobile-menu a').click(function(){
    $('.mega_menu').toggleClass('open');
});
$('.close-menu').click(function(){
  $('.mega_menu').removeClass('open');
});

 })